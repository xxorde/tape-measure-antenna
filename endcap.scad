// Measurement Tape Yagi elemet endcap
// OK1CDJ 2015
// Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
// DM8TE 2022

tape_size=25;

module endcap() {
    difference(){
        hull() {
            rotate([90,90,0]) translate([-4,-2.5,-3]){
                translate([0,5,0])
                rotate ([0,0,0]) cylinder(d=10, h=6);
                rotate ([0,0,0]) cylinder(d=10, h=6);
            }
            cube([tape_size+4,6,10], center=true);
        } 	
        translate([-(tape_size+2)/2,-0.4,-8])
        cube([tape_size+2,0.8,tape_size/2], center=false);	
    }
}

endcap();