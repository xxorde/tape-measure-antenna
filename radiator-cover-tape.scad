//Tape Yagi Radiator Cover
// OK1CDJ 2015
// Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
// DM8TE 2022

tape_size=25;

module cover(){
    difference() {
        cube([tape_size+9,40,11] , center=flase);
        translate([2,0,0])
            cube([tape_size+5,40,9], center=false);
        rotate([0,90,0])
        translate([-3,20,0])
            cylinder(d=10,h=20, center=true, $fs = 1);
    }
}

rotate([180,0,0]) cover();