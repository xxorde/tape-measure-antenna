//Measuerment Tape Yagi Element or Radiator holder
// OK1CDJ 2015
// boom 25 mm dia
// Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
// DM8TE 2022

mast = 25.6;
element = 3.2;
mount_screw=2.4;
holder_screw=3;
tape_size=25;
width = 38;

module holder() {
	difference() {
		minkowski() {
			cube([34,34,tape_size+5], center=true);
			cylinder(r=2,h=1);
		}
        cylinder(d=mast,h=width,center=true);
	
        //mounting screw
        rotate(90,[0,1,0])
        translate([0,0,(width/2+2)*-1])
        cylinder(d=holder_screw,h=1+width/2,center=false);
        rotate(90,[0,1,0])
        translate([-5,-3,(mast/2+4.5)*-1])
        cube([30,6.3,3.1], center=false);
        //magnet
        rotate(90,[1,0,0])
			translate([0,0,(width/2)*-1])
			cylinder(d=8.3,h=3.5,center=false);

        //screws		
		rotate(90,[0,1,0])
		translate([0,-7,mast/2])
		
        //#cube([4,3,mast],center=true);
		cylinder(d=holder_screw,20,center=true);
		rotate(90,[1,0,0])
		translate([mast/2-2.5,0,0])
			cube([5,6,24],center=true);
		
		rotate(90,[0,1,0])
			translate ([0,7,mast/2])
			cylinder(d=holder_screw,20,center=true);

		rotate(90,[1,0,0])
		translate([width/2-0.3,0.5,0])
			cube([1.2,tape_size+0.5,width],center=true);	
	}
}
	

holder();